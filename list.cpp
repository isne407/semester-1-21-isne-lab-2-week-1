#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ) {
		p=head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if(tail==0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
	Node *tmp = new Node(el);
	tail->next = tmp;
	tail = tmp;
	tmp->next = NULL;
	//TO DO!
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if(head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	char el = head->data;
	Node *tmp = head;
	
	while (tmp->next !=tail){
		tmp = tmp->next;
	}

	delete tail;
	tmp = tmp;

	// TO DO!
	return NULL;
}
bool List::search(char el)
{
	Node *tmp = head;
	while(tmp ->next !=NULL){
		if (tmp->data == el){
			return true;
		}

	}
	
	// TO DO! (Function to return True or False depending if a character is in the list.
	return NULL;
}
void List::reverse()
{
	Node* tmp = head;
	Node* prev = NULL, * next = NULL;

	while (tmp != NULL) {
	
		next = tmp->next;
		tmp->next = prev;
		prev = tmp;
		tmp = next;
	}
	head = prev;

	tail->next = tmp;
	tail = tmp;
	tmp->next = NULL;

	// TO DO! (Function is to reverse the order of elements in the list.
}
void List::print()
{
	if(head  == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while(tmp!=tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}